#include "MacierzGraf.hpp"

static int nieskonczonosc = 1000000; 

static int balans = -1;

//konstruktor
GrafMacierz::GrafMacierz(int W, double gestosc) 
	: Graf(W, static_cast<int>(gestosc* W* (W - 1)), gestosc),
	  matrix(std::make_unique<std::unique_ptr<int[]>[]>(W)) {

	//inicjalizacja wszystkich tablic wewnatrz macierzy
	for (int i_komorka = 0; i_komorka < W; ++i_komorka) {

		matrix[i_komorka] = std::make_unique<int[]>(W); 
		for (int j_kolumna = 0; j_kolumna < W; ++j_kolumna) {
			
			if (i_komorka == j_kolumna) 
				matrix[i_komorka][j_kolumna] = 0; //przekatne ustawiamy na zero

			else 
				matrix[i_komorka][j_kolumna] = nieskonczonosc; //kazda inna komorka na nieskonczonosc
		}
	}
}


void GrafMacierz::utworzGraf(const bool czyPetle) const {

	if (Gestosc == 1) {  //przypadek dla calego grafu
		for (int i_wiersz = 0; i_wiersz < W; ++i_wiersz) { //dla kazdego wiersza
			for (int j_kolumna = 0; j_kolumna < W; ++j_kolumna) {  //dla kazdej kolumny
				if (i_wiersz != j_kolumna) //unikniecie tworzenia petli
				{
					int n_waga = rand() % 20  + balans;
					while (n_waga == 0) { //unikniecie wagi rownej 0

						n_waga = rand() % 20 + balans;
					}
					matrix[i_wiersz][j_kolumna] = n_waga;
				}
			}
		}
	}
	else { //inne gestosci

		int i_krawedzi = K;

		while (i_krawedzi) {

			int n_wiersz = rand() % W;
			int n_kolumna = rand() % W;

		
			if (matrix[n_wiersz][n_kolumna] == 0|| matrix[n_wiersz][n_kolumna]== nieskonczonosc) { 

				int n_waga = rand() % 20 + balans;
				while (n_waga == 0) { 

					n_waga = rand() % 20 + balans;
				}

				
				if (n_wiersz != n_kolumna) {

					matrix[n_wiersz][n_kolumna] = n_waga;
					--i_krawedzi;
				}
				else if (czyPetle) {

					matrix[n_wiersz][n_kolumna] = n_waga;
					--i_krawedzi;
				}
			}
		}
	}
}



void GrafMacierz::wyswietlGraf() const {

	std::cout << std::string(33, '-') << "\n  Reprezentacja macierzy sasiedztwa\n\n ";

	
	for (int i_iteracja = 0; i_iteracja < W; ++i_iteracja) {
		
		if (i_iteracja <= 10) std::cout << "    " << i_iteracja;
		else if (i_iteracja <= 100) std::cout << "   " << i_iteracja;
		else std::cout << "  " << i_iteracja;
	}
	std::cout << "\n\n";

	
	for (int i_wiersz = 0; i_wiersz < W; ++i_wiersz) {	
		
		
		if (i_wiersz < 10) std::cout << i_wiersz << "   |";
		else if (i_wiersz < 100) std::cout << i_wiersz << "  |";
		else std::cout << i_wiersz << " |";
		
		
		for (int j_kolumna = 0; j_kolumna < W; ++j_kolumna) {

			int n = matrix[i_wiersz][j_kolumna];

			if (n == nieskonczonosc) std::cout << "*";  
			else std::cout << n;					

			if (abs(n) < 10 || abs(n) == nieskonczonosc) std::cout << "    "; 
			else if (abs(n) < 100) std::cout << "   ";
			else std::cout << "  ";

			if (n < 0) std::cout << '\b';  
		}
		
		std::cout << "|\n";
	}
	std::cout << std::endl;
}



const int GrafMacierz::czytajzPliku() {

	std::ifstream file("PlikWejsciowy.txt"); 
	if (!file.is_open()) {
		throw "Plik wejsciowy z macierza nie wczytany";
		return 1;
	}

	
	int pierwszy, poczatek, koniec, waga;
	file >> K >> W >> pierwszy;
	matrix = std::make_unique<std::unique_ptr<int[]>[]>(W);

	
	for (int i_wiersz = 0; i_wiersz < W; ++i_wiersz) {

		matrix[i_wiersz] = std::move(std::make_unique<int[]>(W));
		for (int j_kolumna = 0; j_kolumna < W; ++j_kolumna) {

			if (i_wiersz == j_kolumna) matrix[i_wiersz][j_kolumna] = 0;
			else matrix[i_wiersz][j_kolumna] = nieskonczonosc;
		}
	}

	
	for (int j_krawedz = 0; j_krawedz < K; ++j_krawedz) {

		file >> poczatek >> koniec >> waga;
		matrix[poczatek][koniec] = waga;;
	}
	file.close();
	return pierwszy; 
}



void GrafMacierz::utworzDane(const int poczatek) const {
	
	std::ofstream file("NoweDaneMatrix.txt");
	if (!file.is_open()) {
		throw  "Błąd czy otwieraniu pliku z macierza";
		return;
	}

	file << K << " " << W << " " << poczatek << "\n";
	for (int i_wiersz = 0; i_wiersz < W; ++i_wiersz) {
		for (int j_kolumna = 0; j_kolumna < W; ++j_kolumna) {

			if (matrix[i_wiersz][j_kolumna] != nieskonczonosc && matrix[i_wiersz][j_kolumna] != 0) {

				file << i_wiersz << " ";
				file << j_kolumna << " ";
				file << matrix[i_wiersz][j_kolumna] << "\n";
			}
		}
	}
	file.close();
}
