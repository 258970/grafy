#ifndef GRAF
#define GRAF

#include <stdlib.h>
#include <iostream>
#include <fstream>


//base abstract class, representaing any kind of graph
class Graf {

protected:
	int W, K; // ilość wierzchołków i krawędzi
	double Gestosc; // gęstość grafu

public:
	const int& getV() const { return W; }				// konstruktor do oczytywania wierzchołków		
	const int& getE() const { return K; }

	virtual void utworzGraf(const bool allowLoops) const = 0; // Tworzenie grafu, wypelnienie go w losowy sposob
	virtual void wyswietlGraf() const =  0; // wyswietla graf na konsoli
	virtual const int czytajzPliku() = 0; // dane z pliku sa przekazywane do programu
	virtual void utworzDane(const int t_startNode) const = 0; //generuje losowe dane do pliku lub do konsoli
													
	virtual ~Graf() {};  // Destruktor
	explicit Graf(int w, int k, double gestosc) : W(w), K(k), Gestosc(gestosc) {} // potrzebne do wykonywania konwersji
	Graf() {}; //konstruktor zeby program mogl stworzyc graf bez inicjalizacji
};				//This is beacause program must be able to crete graph based on the input file infomations.

#endif