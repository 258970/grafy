#ifndef LISTAGRAF
#define LISTAGRAF

#include "graf.hpp"

//represents weighted edge between two nodes
struct Krawedz {
	
	int poczatek, koniec, waga; 
};


//graph in an adjency list representation 
class GrafLista : public Graf {

	Krawedz* krawedz; //tablica wszystkich krawedzi w grafie

public:
	//those methods have comments in the Graph class
	void utworzGraf(const bool czyPetle) const override; // Utworzenie grafu, wypelnienie go w losowy sposob
	void wyswietlGraf() const override; // wyswietlenie grafu na konsoli
	const int czytajzPliku() override; // dane z pliku przekazane do programu
	void utworzDane(const int pierwszy) const override; // generuje losowe dane do pliku lub do konsoli

	const Krawedz* otrzymajStrukture() const { //konstruktor zeby moc uzywac tablicy wszystkich krawedzi
		return krawedz; 
		} 
	void dodajKrawedz(int n_poczat, int n_koniec, int n_waga, int n) const; // definiowanie nowej krawedzi
	bool sprawdzCzyIstnieje(int n_poczat, int n_koniec) const; // sprawdz czy istnieje taki graf
	
	explicit GrafLista(int w, double gestosc)
		:Graf(w, static_cast<int>(gestosc* w* (w - 1)), gestosc),
		 krawedz(new Krawedz[static_cast<int>(gestosc* w* (w - 1))]) {}
	GrafLista() : Graf() {};
	~GrafLista() { delete[] krawedz; }
};


#endif