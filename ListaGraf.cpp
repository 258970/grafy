#include "ListaGraf.hpp"

static int balans = -1; //uzywane do tego zeby random generowal w zakresie (-1;20)

void GrafLista::dodajKrawedz(int n_poczat, int n_koniec, int n_waga, int n) const {

	krawedz[n].koniec = n_koniec;
	krawedz[n].poczatek = n_poczat;
	krawedz[n].waga = n_waga;
}


bool GrafLista::sprawdzCzyIstnieje(int n_poczat, int n_koniec) const {
	for (int i = 0; i < K; ++i) { //sprawdzanie calej listy krawedzi

		if (krawedz[i].koniec == n_koniec && krawedz[i].poczatek == n_poczat) 
			return true; 
	}
	return false;
}


void GrafLista::utworzGraf(const bool czyPetle) const {

	if (Gestosc == 1) {  //calkowicie wypelniony

		int n = 0;
		for (int i = 0; i < W; ++i) { //dla kazdego wierzchołka
			for (int j = 0; j < W; ++j) { //dla wszystkich połączen

				if (i != j) { //uniknięcie tworzenia pętli

					int waga = rand() % 20 + balans;  //tworzenie losowych wag
					while (waga == 0) { //unikniecie wagi rownej 0

						waga = rand() % 20 + balans;
					}
					dodajKrawedz(i, j, waga, n++); //dodanie nowej krawedzi
				}
			}
		}
	}
	else { //inne gestosci

		int i_krawedzi = 0;
		while (i_krawedzi < K) { //indeks krawedzi mniejszy od ilosci krawedzi

			int p_W = rand() % W;	//poczatek wierzcholka
			int k_W = rand() % W;	//koniec wierzcholka
			if (!sprawdzCzyIstnieje(p_W, k_W)) {	//sprawdzamy czy istnieje

				int n_waga = rand() % 20 + balans;
				while (n_waga == 0) {

					n_waga = rand() % 20 + balans;
				}

				if (p_W != k_W) 
					dodajKrawedz(p_W, k_W, n_waga, i_krawedzi++);
				else if (czyPetle) 
					dodajKrawedz(p_W, k_W, n_waga, i_krawedzi++);
			}
		}
	}
}


void GrafLista::wyswietlGraf() const {

	std::cout << "Reprezentacja grafu za pomoca listy sąsiedztwa" << std::endl;
	for (int i = 0; i < W; ++i) {
		std::cout << i; //drukujemy poczatek krawedzi
		for (int j = 0; j < K; ++j) {
			if (krawedz[j].poczatek == i) 
				std::cout << "->" << "[" << krawedz[j].koniec << "|" << krawedz[j].waga << "]"; //drukujemy koniec krawedzi i wage
		}
		std::cout << "\n";
	}
	std::cout << std::endl;
}


const int GrafLista::czytajzPliku() {
	std::ifstream file("dane.txt");
	if (!file.is_open()) {
		throw "Plik nie został otwarty";
		return 1;
	}

	//zmienne i inicjalizacja tablicy
	int pierwszy, poczatek, koniec, waga;
	file >> K >> W >> pierwszy;
	krawedz = new Krawedz[K];

	//dodajemy wszystkie połączenia(tworzymy wszystkie krawedzie)
	for (int j = 0; j < K; ++j) {
		file >> poczatek >> koniec >> waga;
		dodajKrawedz(poczatek, koniec, waga, j);
	}
	file.close();
	return pierwszy; //zwracamy wierzcholek startowy
}


void GrafLista::utworzDane(const int pierwszy) const {

	std::ofstream file("StworzoneDane.txt");
	if (!file.is_open()) {
		throw  "Nie można otworzyć pliku";
		return;
	}

	file << K << " " << W << " " << pierwszy << "\n";
	for (int j = 0; j < K; ++j) {
		//zapisujemy dane o kazdej krawedzi
		file << krawedz[j].poczatek << " ";
		file << krawedz[j].koniec << " ";
		file << krawedz[j].waga << "\n";		
	}
	file.close();
}
