#ifndef BELLMANFORD
#define BELLMANFORD

#include <chrono>
#include <string> 

#include "ListaGraf.hpp"
#include "MacierzGraf.hpp"


double bellmanFord(std::shared_ptr<GrafLista> n_graf, int pierwszy, bool drukujWynik);

double bellmanFord(std::shared_ptr<GrafMacierz> n_graf, int pierwszy, bool drukujWynik);

#endif