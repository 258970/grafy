#ifndef MACIERZGRAF
#define MACIERZGRAF

#include "graf.hpp" 

#include <cstdlib>
#include <memory>


class GrafMacierz : public Graf {

	std::unique_ptr<std::unique_ptr<int[]>[]> matrix; //dynamic array of dynamic arrays; 
														//main container of graph
public:
	//those methods have comments in the Graph class
	void utworzGraf(const bool czyPetle) const override;
	void wyswietlGraf() const override;
	const int czytajzPliku() override;
	void utworzDane(const int poczatek) const override;

	//returns value of given cell in matrix array
	const int& zwrocWage(int i_wiersz, int i_kolumna) const { 
		return matrix[i_wiersz][i_kolumna]; 
		} 

	explicit GrafMacierz(int w, double gestosc);
	GrafMacierz() : Graf() {};
};

#endif