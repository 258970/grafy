#include "bellmanford.hpp"

static int  nieskonczonosc = 1000000; 
static int  minusNieskonczonosc = -1000000;	
										


void rozwiazanie(std::string t_sciezka[], int t_droga[], int t_W, int pierwszy) {

	std::ofstream file("Output.txt");
	
	std::cout << "Koncowe rozwiazanie\n\n";
	std::cout << "Wierzchołek startowy -> " << pierwszy << "\n\n";

	file <<"Koncowe rozwiazanie\n\n";
	file << "Wierzchołek startowy -> " << pierwszy << "\n\n";

	for (int i = 0; i < t_W; ++i) {
		 
		if (t_droga[i] == minusNieskonczonosc) {
			std::cout << i << "koszt ->" << "-inf\n";
			file << i << "koszt ->" << "-inf\n";
			continue; 
		}																
		else if (t_droga[i] == nieskonczonosc) {
			std::cout << i << "koszt ->" << "inf\n";
			file << i << "koszt ->" << "inf\n";
			continue; 
		}
		
		else {
			std::cout << i << "koszt ->" << t_droga[i] << std::endl;
			file << i << "koszt ->" << t_droga[i] << std::endl;
		}	
		
		std::cout << "Najkrótsza ścieżka: " << t_sciezka[i] << i;
		file << "Najkrótsza ścieżka: " << t_sciezka[i] << i;
		
		std::cout << std::endl;
		file << std::endl;
	}
	std::cout << std::endl;
	file.close();
}



double bellmanFord(std::shared_ptr<GrafLista> n_graf, int pierwszy, bool drukujWynik) {

	std::string* przechowujSciezke = new std::string[n_graf->getV()]; 

	auto t_start = std::chrono::high_resolution_clock::now(); 

	int* przechowujOdleglosc = new int[n_graf->getV()]; 
	
	for (int i = 0; i < n_graf->getV(); ++i) {

		przechowujOdleglosc[i] = nieskonczonosc; 
	}

	przechowujOdleglosc[pierwszy] = 0; 

	for (int i = 1; i < n_graf->getV(); ++i) { 
		for (int j = 0; j < n_graf->getE(); ++j) { 

			int u = n_graf->otrzymajStrukture()[j].poczatek;
			int v = n_graf->otrzymajStrukture()[j].koniec;
			int waga = n_graf->otrzymajStrukture()[j].waga;

			if (przechowujOdleglosc[u] + waga < przechowujOdleglosc[v]) { 
				przechowujOdleglosc[v] = przechowujOdleglosc[u] + waga;
					
				if (drukujWynik) { 
						
					przechowujSciezke[v].clear();
					przechowujSciezke[v].append(przechowujSciezke[u] + std::to_string(u) + "->");
				}
			}
		}
	}

	
	for (int i = 1; i < n_graf->getV(); ++i) {
		for (int j = 0; j < n_graf->getE(); ++j) {
			
			int u = n_graf->otrzymajStrukture()[j].poczatek;
			int v = n_graf->otrzymajStrukture()[j].koniec;
			int waga = n_graf->otrzymajStrukture()[j].waga;
			if (przechowujOdleglosc[u] + waga < przechowujOdleglosc[v]) {
				if (przechowujOdleglosc[u] > nieskonczonosc/2) 
					przechowujOdleglosc[u] = nieskonczonosc; 
				else 
					przechowujOdleglosc[v] = minusNieskonczonosc;				
			}
			else if (przechowujOdleglosc[u] > nieskonczonosc/2) 
				przechowujOdleglosc[u] = nieskonczonosc;  
		}
	}
	auto t_end = std::chrono::high_resolution_clock::now(); 

	
	if (drukujWynik) 
		rozwiazanie(std::move(przechowujSciezke), std::move(przechowujOdleglosc), n_graf->getV(), pierwszy);
	delete[] przechowujOdleglosc;
	delete[] przechowujSciezke;
	return std::chrono::duration<double, std::milli>(t_end - t_start).count(); 
}



double bellmanFord(std::shared_ptr<GrafMacierz> n_graf, int pierwszy, bool drukujWynik) {
	
	std::string* przechowujSciezke = new std::string[n_graf->getV()];

	auto t_start = std::chrono::high_resolution_clock::now(); 

	int* przechowujOdleglosc = new int[n_graf->getV()];

	for (int i = 0; i < n_graf->getV(); ++i) {

		przechowujOdleglosc[i] = nieskonczonosc;
	}

	przechowujOdleglosc[pierwszy] = 0;

	for (int i = 1; i < n_graf->getV(); ++i) {
		for (int j = 0; j < n_graf->getV(); ++j) {
			for (int w = 0; w < n_graf->getV(); ++w) {

				int u = j;
				int v = w;
				int waga = n_graf->zwrocWage(j, w);
				if (przechowujOdleglosc[u] + waga < przechowujOdleglosc[v]) {
					
					przechowujOdleglosc[v] = przechowujOdleglosc[u] + waga;
					if (drukujWynik) {

						przechowujSciezke[v].clear();
						przechowujSciezke[v].append(przechowujSciezke[u] + std::to_string(u) + "->");
					}
				}
			}
		}
	}
	for (int i = 1; i < n_graf->getV(); ++i) {
		for (int j = 0; j < n_graf->getV(); ++j) {
			for (int w = 0; w < n_graf->getV(); ++w) {

				int u = j;
				int v = w;
				int waga = n_graf->zwrocWage(j, w);
				if (przechowujOdleglosc[u] + waga < przechowujOdleglosc[v]) {
				
					if (przechowujOdleglosc[u] > nieskonczonosc / 2) 
						przechowujOdleglosc[u] = nieskonczonosc;
					else if (waga == nieskonczonosc) 
						continue; 
					else 
						przechowujOdleglosc[v] = minusNieskonczonosc;
				}
				else if (przechowujOdleglosc[u] > nieskonczonosc/2) 
					przechowujOdleglosc[u] = nieskonczonosc;	
			}
		}
	}
	auto t_end = std::chrono::high_resolution_clock::now(); 

	if (drukujWynik) 
		rozwiazanie(std::move(przechowujSciezke), std::move(przechowujOdleglosc), n_graf->getV(), pierwszy);
	delete[] przechowujOdleglosc;
	delete[] przechowujSciezke;
	return std::chrono::duration<double, std::milli>(t_end - t_start).count(); 
}
