#include <time.h>

#include "bellmanford.hpp"



template<typename T>
std::ostream& testy(std::ostream& file, int (&wierzchTab)[5], double (&gestoscTab)[4], int n_petli) {
	
	for (double gestosc : gestoscTab) { 
		for (int wierzcholki : wierzchTab) { 
			double wynik = 0;
			for (int i = 0; i < n_petli; ++i) { 

				std::shared_ptr<T> graph = std::make_shared<T>(wierzcholki, gestosc); 
				int start = rand() % wierzcholki; 

				graph->utworzGraf(true); 

				wynik += bellmanFord(std::move(graph), start, false); 
				std::cout << (i * 100 / n_petli) + 1 << "%" << "\r" << std::flush;
			}
			file << wynik / n_petli << " "; 
			std::cout << wierzcholki << " WIERZCHOŁKÓW" << std::endl;
		}
		std::cout << "\n" << gestosc << " GĘSTOŚĆ\n" << std::endl;
	}
	file << "\n";
	return file;
}


int main() {

	srand(static_cast<unsigned int>(time(NULL)));

	bool CzyTestowac = false; 
	

	

	if (CzyTestowac) {
		
		
		int wierzchTab[5] = {10, 50, 100, 250, 500}; 
		double gestoscTab[4] = {0.25, 0.5, 0.75, 1}; 
		int n_petli = 10;		

		std::ofstream file("CzasyOutMain.txt");
		if (!file.is_open()) {

			std::cerr << "Plik do zapisu czasów nie został otwarty" << std::flush;
			return 1;
		}

		testy<GrafLista>(file, wierzchTab, gestoscTab, n_petli); 
		std::cout << "Testowanie dla list skonczone \n" << std::endl;
		testy<GrafMacierz>(file, wierzchTab, gestoscTab, n_petli);	
		std::cout << "Testowanie dla macierzy skonczone" << std::endl;

		file.close();
		return 0;
	}

	typedef	GrafLista ActualGraph;
	bool uzyjPliku = false;
	bool czyPetla = true;
	int wierzcholki = 7;	
	double gestosc = 0.5;
	int start = 2;
	

	std::shared_ptr<ActualGraph> graph;

	if (uzyjPliku) { 
		std::shared_ptr<ActualGraph> tmp = std::make_shared<ActualGraph>();
		graph = tmp;
		try {
			start = tmp->czytajzPliku(); 
		}
		catch (const char* informacja) {
			std::cerr << informacja << std::flush;
			return 1;
		}
	}
	else { 
		std::shared_ptr<ActualGraph> tmp = std::make_shared<ActualGraph>(wierzcholki, gestosc);
		graph = tmp;
		tmp->utworzGraf(czyPetla); 
	}

	graph->wyswietlGraf(); 
	try {
		if (!uzyjPliku) graph->utworzDane(start); 
	}
	catch(const char* informacja) {
		std::cerr << informacja << std::flush;
		return 1;
	}
	bellmanFord(std::move(graph), start, true); 

	return 0;
}